#include <string>
#include <iostream>
#include <vector>

using std::string;

//class creation for a 'Spot'
class Spot
{
	public:
	Spot();
	Spot(const string name, const string coordinates, const int radius, const string descr);
	string name;
	string coordinates;
	int radius;
	string descr;
	
	private:
	;
};

//------------------------------------------------------------------------
//Spot 1,34QCH6325382059, 10KM, "A Random spot on the globe"
//Spot 2,49VDC1933169122, 4000M, "Another random spot on the globe"
//Spot 3,24XVK5252656544, 6.7KM, "Yet another random spot on the globe"
//------------------------------------------------------------------------

Spot::Spot()
{
}

//Spot main
int main(int argc, char *argv[])
{
	Spot spot1;
	Spot spot2;
	
	spot1.name = "Here1";
	spot1.coordinates = "1234";
	spot1.radius = 5280;
	spot1.descr = "Random spot";
	
	spot2.name = "There1";
	spot2.coordinates = "4321";
	spot2.radius = 825;
	spot2.descr = "Spot Random";
	
	std::vector<Spot> spots;
	spots.push_back(spot1);
	spots.push_back(spot2);
	
	for ( auto spot : spots)
	{
		std::cout << "Spot Name: " << spot.name << " Spot Coordinates: " << spot.coordinates << " Radius: " << spot.radius << " Description: " << spot.descr << std::endl;
	}
	return 0;
}
