#include <ctime>
#include <string>
#include <iostream>

using std::string;
int current_year()
{
    time_t now = time(NULL);
    tm *formatted = gmtime(&now);
    return formatted->tm_year + 1900;
}
class Person
{
public:
    Person();
    Person(const string first_name, const int year_born);
    int DOB;
    void first_name(const string &new_first_name);
    string first_name() const;
    // age calculation
    int age();

private:
    string _first_name;
};

//age cacl continued
int Person::age() const
{
	return current_year() - DOB;
}


Person::Person()
{
}

Person::Person(const std::string first_name, const int year_born)
{
    DOB = year_born;
    _first_name = first_name;
}
string Person::first_name() const
{
	return _first_name;
}



int main(int argc, char *argv[])
{
    Person person1("Alice", 1980);
    Person person2;
    person2.DOB = 1986;
    person2._first_name("Bob");
    std::vector<Person> people;
    people.push_back(person1);
    people.push_back(person2);
    
    //for (i+0; i < people.size(); i++
    //{people[i]  <--- Incomplete!
    
    for ( auto person : people)
    {
		std::cout << "First Name: " << person.first_name() << " DOB: " << person.DOB << std::endl;
	}

    return 0;
}

