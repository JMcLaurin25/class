
#include <iostream>
#include <string>
using std::string;
static const int ARRAY_SIZE = 100;

void init_array(int *data, int size, int init_value);
void dump_array(int *data, int size);

int main(int argc, char *argv[])
{
	int ARRAY_FUN;
	std::cout << "How large would you like to make the list: ";
	std::cin >> ARRAY_FUN;
	int boomArray[ARRAY_FUN];
	const int init_value = 2;
	init_array(boomArray, ARRAY_SIZE, init_value);
	dump_array(boomArray, ARRAY_SIZE);
}

void init_array(int *data, int size, int init_value)
{
	int i;
	for (i=0; i < size; i++)
	{
		data[i] = init_value;
		std::cout << i;
	}
}

void dump_array(int *data, int size)
{
	int i;
	std::cout << "Dumpping Array of size: " << size << std::endl;
	for (i=0;i<size;i++)
	{
		std::cout << data[i] << " ";
	}
	std::cout << std::endl;
}



