
#include <iostream>
#include <string>
using std::cout;
using std::string;
static const int ARRAY_SIZE = 50;

void init_array(int *data, int size, int init_value);
void dump_array(int *data, int size);

int main(int argc, char *argv[])
{
	int boomArray[ARRAY_SIZE];
	const int init_value = 2;
	init_array(boomArray, ARRAY_SIZE, init_value);
	dump_array(boomArray, ARRAY_SIZE);
}

void init_array(int *data, int size, int init_value)
{
	int i;
	for (i=0; i < size; i++)
	{
		data[i] = init_value;
	}
}

void dump_array(int *data, int size)
{
	int i;
	std::cout << "Dumpping Array of size: " << size << std::endl;
	for (i=0;i<size;i++)
	{
		std::cout << data[i] << " ";
	}
	std::cout << std::endl;
}



