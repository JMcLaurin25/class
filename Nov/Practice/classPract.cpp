#include <iostream>

class Banana{
public:
    Banana(){}
    Banana(int xNew);
    void makeBanana(int xNew, int yNew, int zNew);
    int getX();
    int getY();
    int getZ();
private:
    int x;
    int y;
    int z;
};

Banana::Banana(int xNew)
{
    x = xNew;
    y = 0;
    z = 0;
}

void Banana::makeBanana(int xNew, int yNew, int zNew)
{
    x = xNew;
    y = yNew;
    z = zNew;

}

int Banana::getX()
{
    return x;
}
int Banana::getY()
{
    return y;
}
int Banana::getZ()
{
    return z;
}

int main()
{
    Banana smallB;
    
    smallB.makeBanana(10,11,12);
    std::cout << smallB.getX() << " " << smallB.getY() << " " << smallB.getZ() << ":" << std::endl; 

}

