/* Compute an array (50 Elements) with random numbers
 * which outputs the square root and cube roots in JSON 
 * (JavaScript Object Notation). Random integers to be
 * between 1 - 107
*/

#include <iostream>
#include <string>
#include <random>
#include <vector>
#include "jsonVector.h"
using namespace std;

int main()
{
    const int maxCount=50;
    string filename;
    vector<int> numSet;
    vector<DataSet> fullSet;
    DataSet newData;
    int numbRand;

    for (int i=0; i<maxCount; ++i)
    {
        numbRand = rand() % 107 + 1;
        newData.makeDataset(numbRand);
        fullSet.push_back(newData);
    }

    outputNum(fullSet);

    return 0;
    }
