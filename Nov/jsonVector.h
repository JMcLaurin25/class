#ifndef JSONVECTOR_H
#define JSONVECTOR_H
#include <vector>
using std::vector;

class DataSet
{
    public:
        DataSet(int num);
        DataSet(){}
        void makeDataset(int num);
        int randNum;
        double sqnum;
        double cbnum;
    
};

void outputNum(vector<DataSet> fullSet);
vector<double> vectSet(int curValue);

#endif
