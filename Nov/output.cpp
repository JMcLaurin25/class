#include <string>
#include <iomanip>
#include <fstream>
#include <vector>
#include "jsonVector.h"
using namespace std;

void outputNum(vector<DataSet> fullSet)
{
    ofstream myfile;
    string filename = "example.json";
    myfile.open(filename);
    myfile << "[";        
    for (int i=0; i<fullSet.size(); ++i)
    {
        int curRand = fullSet[i].randNum;
        double sqnum = fullSet[i].sqnum; 
        double cbnum = fullSet[i].cbnum;

    myfile << "{" << "\"number\": " << curRand << ", "
             << "\"Square Root\": " << std::setprecision(2) << fixed << "\"" << sqnum << "\"" << ", "
             << "\"Cube Root\": " << "\"" << cbnum << "\"" << "}";

    if (i <(fullSet.size() - 1))
            {
    myfile << "," << endl;
        }
        }
    myfile << "]" << endl;
    myfile.close();
}
