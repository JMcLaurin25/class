/* Compute an array (50 Elements) with random numbers
 * which outputs the square root and cube roots in JSON 
 * (JavaScript Object Notation). Random integers to be
 * between 1 - 107
*/

#include <iostream>
#include <string>
#include <random>
#include <cmath>
#include <iomanip>
#include <fstream>
using namespace std;

int main()
{
    int randNum;
    const int maxCount=50;
    int numSet [maxCount];
    int num;
    string filename;

    for (int i=0; i<maxCount; ++i)
        {
        randNum = rand() % 107 + 1;        
        numSet[i] =randNum;
        }


    //Output to file
    ofstream myfile;
    cout << "Provide filename with format. \"example.json\"" << endl;
    cin >> filename;

    myfile.open(filename);
    myfile << "{\"Random Numbers\":[" << endl;
    for (int i=0; i<maxCount; ++i)
        {
        double sqnum = sqrt(numSet[i]);
	double cbnum = cbrt(numSet[i]);
        myfile << "  {" << "\"number\":" << numSet[i] << ", "
             << "\"Square Root\":" << std::setprecision(2) << fixed << sqnum << ", "
             << "\"Cube Root\":" << cbnum << "}" << endl;
        }
    myfile << "]}" << endl;
    myfile.close();
    return 0;
} 
        
