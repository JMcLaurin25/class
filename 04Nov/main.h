#include <iostream>
#include <string>

#ifndef MAIN_H_
#define MAIN_H_

class Rotor
{
private:
    int ringSetting;
    int _winPos;
    int turnoverPos;
    std::vector<int> forwardOffset;
    std::vector<int> reverseOffset;

public:
    //Rotor(){};
    //winPos();
    //forwardEnc();

};

int charInt(char ch);
char intChar(int ch);
std::vector<int> forward_shift(const std::string rotorValues);
std::vector<int> reverse_shift(const std::string rotorValues);

#endif /* MAIN_H_ */
