/*
 * Project for creating rotors for an Eniogma Machine
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "main.h"

//Convert from character to integer
int charInt(char ch)
{
    int value = ((int)ch - (int)'A');
    return value;
}

//Convert from integer to character
char intChar(int ch)
{
    char value = (char)(ch + (int)'A');
    return value;
}

//Function for correcting the offset.
int offCorr(int offset)
{
    int value;
    if(offset > 25)
    {
        value = offset - 25;
    }
    else if(offset < 0)
    {
        value = 25 + offset;
    }
    return value;
};

//Encryption function Forward
char encrChar(char ch, std::vector<int> shifts)
{
    char output;
    int curCh = charInt(ch);
    int corrOffset = curCh + shifts[curCh];
    int finalOffset = corrOffset;

    output = intChar(finalOffset);

    return output;
}

std::string stringProcess(std::string content, std::vector<int> shifts)
{
    char outputArray[content.length()];
    char ch;
    char value;

    for (int i=0; i<content.length(); i++)
        {
            ch = toupper(content[i]);
                value = encrChar(ch, shifts);
                outputArray[i] = value;
        }
    std::string output(outputArray);

    return output;
}

int main()
{
    std::string rotor1, rotor2, rotor3, valueIn, valueInR, encrypValue, encrypValueR;
    std::vector<int> shifts1, shifts2, shifts3, shifts1R, shifts2R, shifts3R;

    rotor1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
    /*rotor2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
    rotor3 = "BDFHJLCPRTXVZNYEIWGAKMUSQO";*/

    shifts1 = forward_shift(rotor1);
    shifts1R = reverse_shift(rotor1);
    /*shifts2 = forward_shift(rotor2);
    shifts2R = reverse_shift(rotor2);
    shifts3 = forward_shift(rotor3);
    shifts3R = reverse_shift(rotor3);*/

    /*
     * String output rep}
//Encryption function Reverse
char encrCharR(char ch, std::vector<int> shifts)
{
    char output;
    int curCh = charInt(ch);
    int corrOffset = shifts[curCh] - curCh;
    int finalOffset = corrOffset;

    output = intChar(finalOffset);

    return output;
}laces spaces with '('
     * Create condition for null char
     */
    valueIn = "Corrections";

    encrypValue = stringProcess(valueIn, shifts1);
    encrypValueR = stringProcess(encrypValue, shifts1R);

    std::cout << encrypValue << std::endl;
    std::cout << encrypValueR << std::endl;
}
