#include <vector>
#include "main.h"

//Vector for forward offsets
std::vector<int> forward_shift(const std::string rotorValues)
{
    std::vector<int> forwardOffset;

    for (int i=0; i < 26; i++)
    {
        int arValue = (charInt(rotorValues[i]) - i);
        forwardOffset.push_back(arValue);
    };

    return forwardOffset;
}

//Vector for reverse offsets
std::vector<int> reverse_shift(const std::string rotorValues)
{
    std::vector<int> reverseOffset(26);

    for (int i=0; i < 26; i++)
    {
        int arValue = (i - charInt(rotorValues[i]));
        int spot = charInt(rotorValues[i]);

        reverseOffset[spot] = arValue;
    };

    return reverseOffset;
}
