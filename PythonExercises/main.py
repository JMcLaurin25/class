import sum
import even_sum

def main():
    data = [3,7,4,99,64]
    print "\n\tThe sum of %s is %d" % (data, sum.sum(data))
    
    print "\tThe average of even numbers is %d" % even_sum.even_sum(data)

if __name__ == '__main__':
    main()
