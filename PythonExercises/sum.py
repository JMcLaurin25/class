
def sum(nums):
    result = 0
    for num in nums:
        result += num
    return result
    
def test_sum():
    import sys
    test_data = [1,2,3]
    if sum(test_data) != 6:
        print "Self test failed!"
        sys.exit(1)
    print "Self test Passed"
    sys.exit(0)
if __name__ == '__main__':
    test_sum()
