class MenuItem():
    def __init__(self, name, price):
        self.name = name
        self.price = price

class Restaurant():
    def __init__(self, name, rating):
        self.name = name
        self.rating = rating
        self.menu = {}
        
        #----------ERROR SOMEWHERE IN HERE.
        #----------Missing 4th iteration for 'restaurants' see line 55...ish
        self.restaurants = []
        
        
    def newItem(self, newItem):
        for newItem in newItem:
            self.menu[newItem.name] = newItem
    def avePrice(self):
        num_items = 0
        total_price = 0.00
        for newItem in newItem:
            total_price += newItem.price
            num_items += 1
        return total_price / num_items
    def output(self):
        result = '%s * %s $ %s' % (self.name, self.rating, avePrice)
        
class YelpMain():
    def __init__(self, restaurants=None):
        self.restaurants = {}
        if restaurants is not None:
            for restaurant in restaurants:
                self.restaraunts[restaurant.name] = restaurant
    def print_restaurant(self):
        for restaurant in self. restaurants.values():
            print restaurant.output()
    def find_food(self, food_name):
        results = []
        for restaurant in self.restaurant.menu:
            results.append((restaurant.name, restaurant.menu[food_name].price))
        return results
    def print_prices(self, food_name):
        for pair in self.find_food(food_name):
            print '%s %f' % pair
    def __str__(self):
        return ''
                    
def main():
    #restaurants = [Arbys, BurgerKing, McDonalds, Wendys]
    
    #restaurants.Arbys = {RoastBeef Sandwich: 5.00, milkshake: 1.25, rating: 5}
    #restaurants.BurgerKing = {burger: 5.25, milkshake: 2.00, rating: 4}
    #restaurants.McDonalds = {burger: 6.50, rating: 1}
    #restaurants.Wendys = {burger: 3.75, milkshake: 2.30, rating: 2}
    
    arbys = Restaurant('Arbys', [MenuItem('Roast Beef Sandwich', 5.00),
                                 MenuItem('Milkshake', 1.25),
                                 ],
                                 5)
    burger_king = Restaurant('Burger King', [MenuItem('Burger', 5.25),
                                             MenuItem('Milkshake', 2.00),
                                             ],
                                             4)
    mcdonalds = Restaurant('McDonalds', [MenuItem('Burger', 6.50),
                                         ],
                                         1)
    wendys = Restaurant('Wendys', [MenuItem('Burger', 3.75),
                                   MenuItem('Milkshake', 2.30),
                                   ],
                                   2)
    
    print YelpMain([arbys, burger_king, mcdonalds, wendys])
    
if __name__ == '__main__':
    main()
