# get 6 extra points for 3 vowels in a row in addition to regularly calculated points.
# get 10 extra points if any sequence of every 4th letter has a run "match", 3 times.

def main():
    
    print "To calculate the value of a word with \nVowels = 2 points and Consanants = 1 point, "
    
    while True:
        word = raw_input("\nProvide Word for point value: ")
        print "\n\tPoint Value: %d" % ComputeScore(word)

def ComputeScore(word):
    value = 0
    BonusValue = 0
    prevch = 0

    for i in range(len(word)):
        ch = word[i]
        
        if is_vowel(ch):
            CharValue = 2
            #-----------------------------------------------------------
            if ch == prevch:
                count = count + 1
            else:
                count = 1
            if count == 3:
                BonusValue = 6
            else:
                BonusValue = 0
            prevch = ch
            #-----------------------------------------------------------
            print "\tCurrent ch: %s - Value: %d - Bonus: %d - Count: %d" % (ch, CharValue, BonusValue, count)
            print "\t\tPrevious Ch: %s" % prevch
        elif is_cons(ch):
            CharValue = 1
            print "\tCurrent ch: %s - Value: %d - Bonus: %d - Count: %d" % (ch, CharValue, BonusValue, count)
            print "\t\tPrevious Ch: %s" % prevch
        else:
            CharValue = 0
        if i % 5 == 4:
            CharValue = CharValue * 3
        prevch = ch
        value = value + CharValue + BonusValue


    return value
    
        
    
def is_vowel(ch):
    if ch in 'aeiouAEIOU':
        return True
    return False
    
def is_cons(ch):
    if ch in 'bcdfghjklkmnpqrstvwxyzBCDFGHJKLKMNPQRSTVWXYZ':
        return True
    return False

if __name__ == '__main__':
    main()
