# Every vowel = 2 points
# Every Consanant = 1 point
# Every fifth character * 3

def main():
    
    print "To calculate the value of a word with \nVowels = 2 points and Consanants = 1 point, "
    word = raw_input("Provide Word for point value: ")
    
    print "Point Value: %d" % ComputeScore(word)

def ComputeScore(word):
    value = 0
    
    for i in range(len(word)):
        ch = word[i]
        
        if is_vowel(ch):
            CharValue = 2
        elif is_cons(ch):
            CharValue = 1
        else:
            CharValue = 0
             
        if i % 5 == 4:
            CharValue = CharValue * 3
            
        value = value + CharValue
        
    return value
    
def is_vowel(ch):
    if ch in 'aeiouAEIOU':
        return True
    return False
    
def is_cons(ch):
    if ch in 'bcdfghjklkmnpqrstvwxyzBCDFGHJKLKMNPQRSTVWXYZ':
        return True
    return False

if __name__ == '__main__':
    main()
