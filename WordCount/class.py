import math
import random
#TODO: Modify this program so it prints the average number of intersections for 
#every maxium radius between 1 and 10
class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    def dist(self, other):
        deltax = other.x - self.x
        deltay = other.y - self.y
        
        distance = math.sqrt((deltax * deltax) + (deltay * deltay))
        return distance

class Circle():
    def __init__(self, radius, origin):
        self.radius = radius
        self.origin = origin
        
    def is_in_circle(self, test_point):
        if self.origin.dist(test_point) > self.radius:
            return False
        return True
    
    def intersect(self, other_circle):
        combined_radii = self.radius + other_circle.radius
        if self.origin.dist(other_circle.origin) > combined_radii:
            return False
        return True
        
    def __str__(self):
        result = '(%d, %d) Radius %d' % (self.origin.x, self.origin.y, self.radius)
        return result
    
def main():
    min_xy = -5
    max_xy = 5
    min_radius = 0
    max_radius = 1
        
    while max_radius != 10:
        
        intersection_count = 0
        for i in range(1000):
        
            #circles list--------------------------------------
            circles = []
            for i in range(10):
                x = random.randint(min_xy, max_xy)
                y = random.randint(min_xy, max_xy)
                #if we are to average the intersections for all radii up to 10,
                #we will need to make radius a non-random int
                #radius = random.randint(min_radius, max_radius)
                radius = max_radius
                origin = Point(x, y)
                
                circle = Circle(radius, origin)
                circles.append(circle)
                #print circle
            
            intersection_count += num_intersections(circles)
            
        average = intersection_count / 1000
            
        print "Number of intersections with radius of %d: %d" % (max_radius, intersection_count)
        print "\t %d / 1000 = %d average intersections" % (intersection_count, average)
        
        max_radius = max_radius + 1
    
def num_intersections(circles):
    result = 0
    for i in range(0,len(circles) - 1):
        for j in range(i + 1, len(circles)):
            circle1 = circles[i]
            circle2 = circles[j]
            if circle1.intersect(circle2):
                result = result + 1
    return result
        
        
if __name__ == '__main__':
    main()
        
