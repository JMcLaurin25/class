//Average, min, max
//Minutes

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
using namespace std;

string timeConv(int tValue)
{
    int seconds = tValue % 60;
    int minutes = tValue / 60;
    stringstream convTime;
    convTime << minutes << ":" << seconds;
    return convTime.str();
}

void average(vector<int> input)
{
    int inputSum = 0;
    int inputAve;
    
    for (auto i: input)
    {
        inputSum += i;
    }
    
    inputAve = inputSum / input.size();
    
    std::cout << "\tAverage: " << timeConv(inputAve) << std::endl;
}

void max(vector<int> input)
{
    int maxNum = 0;
    for (auto i: input)
    {
        if (i > maxNum)
        {
         maxNum = i;
        }
     }
     std::cout << "\tMax: " << timeConv(maxNum) << std::endl;
}

void min(vector<int> input)
{
    int minNum = 9000;
    for (auto i: input)
    {
        if (i < minNum)
        {
         minNum = i;
        }
     }
     std::cout << "\tMin: " << timeConv(minNum) << std::endl;
}

int main()
{
    map<string, vector<int> > soldiers;
    
    soldiers["Joe"].push_back(900);
    soldiers["Joe"].push_back(925);
    soldiers["Joe"].push_back(875);
    
    soldiers["Jane"].push_back(950);
    soldiers["Jane"].push_back(1000);
    soldiers["Jane"].push_back(1025);
    soldiers["Jane"].push_back(975);
    
    soldiers["Billy"].push_back(850);
    soldiers["Billy"].push_back(875);
    soldiers["Billy"].push_back(925);
    soldiers["Billy"].push_back(900);
    soldiers["Billy"].push_back(850);

    soldiers["Sarah"].push_back(1050);
    soldiers["Sarah"].push_back(1075);
    
    for (auto iter : soldiers)
    {
        std::cout << iter.first << ": " << std::endl;
        average(iter.second);
        max(iter.second);
        min(iter.second);
        std:;cout << "-------------------------------" << std::endl;
    }
    
    return 0;
}
