# CMake generated Testfile for 
# Source directory: /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35
# Build directory: /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(pcre_test "sh" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_test.sh")
ADD_TEST(pcre_grep_test "sh" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_grep_test.sh")
ADD_TEST(pcrecpp_test "pcrecpp_unittest")
ADD_TEST(pcre_scanner_test "pcre_scanner_unittest")
ADD_TEST(pcre_stringpiece_test "pcre_stringpiece_unittest")
