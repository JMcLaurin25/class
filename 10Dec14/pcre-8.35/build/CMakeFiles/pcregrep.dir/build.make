# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build

# Include any dependencies generated for this target.
include CMakeFiles/pcregrep.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/pcregrep.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/pcregrep.dir/flags.make

CMakeFiles/pcregrep.dir/pcregrep.c.o: CMakeFiles/pcregrep.dir/flags.make
CMakeFiles/pcregrep.dir/pcregrep.c.o: ../pcregrep.c
	$(CMAKE_COMMAND) -E cmake_progress_report /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building C object CMakeFiles/pcregrep.dir/pcregrep.c.o"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -o CMakeFiles/pcregrep.dir/pcregrep.c.o   -c /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcregrep.c

CMakeFiles/pcregrep.dir/pcregrep.c.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing C source to CMakeFiles/pcregrep.dir/pcregrep.c.i"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -E /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcregrep.c > CMakeFiles/pcregrep.dir/pcregrep.c.i

CMakeFiles/pcregrep.dir/pcregrep.c.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling C source to assembly CMakeFiles/pcregrep.dir/pcregrep.c.s"
	/usr/bin/cc  $(C_DEFINES) $(C_FLAGS) -S /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcregrep.c -o CMakeFiles/pcregrep.dir/pcregrep.c.s

CMakeFiles/pcregrep.dir/pcregrep.c.o.requires:
.PHONY : CMakeFiles/pcregrep.dir/pcregrep.c.o.requires

CMakeFiles/pcregrep.dir/pcregrep.c.o.provides: CMakeFiles/pcregrep.dir/pcregrep.c.o.requires
	$(MAKE) -f CMakeFiles/pcregrep.dir/build.make CMakeFiles/pcregrep.dir/pcregrep.c.o.provides.build
.PHONY : CMakeFiles/pcregrep.dir/pcregrep.c.o.provides

CMakeFiles/pcregrep.dir/pcregrep.c.o.provides.build: CMakeFiles/pcregrep.dir/pcregrep.c.o

# Object files for target pcregrep
pcregrep_OBJECTS = \
"CMakeFiles/pcregrep.dir/pcregrep.c.o"

# External object files for target pcregrep
pcregrep_EXTERNAL_OBJECTS =

pcregrep: CMakeFiles/pcregrep.dir/pcregrep.c.o
pcregrep: CMakeFiles/pcregrep.dir/build.make
pcregrep: libpcreposix.a
pcregrep: /usr/lib/x86_64-linux-gnu/libz.so
pcregrep: /usr/lib/x86_64-linux-gnu/libbz2.so
pcregrep: libpcre.a
pcregrep: CMakeFiles/pcregrep.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking C executable pcregrep"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/pcregrep.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/pcregrep.dir/build: pcregrep
.PHONY : CMakeFiles/pcregrep.dir/build

CMakeFiles/pcregrep.dir/requires: CMakeFiles/pcregrep.dir/pcregrep.c.o.requires
.PHONY : CMakeFiles/pcregrep.dir/requires

CMakeFiles/pcregrep.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/pcregrep.dir/cmake_clean.cmake
.PHONY : CMakeFiles/pcregrep.dir/clean

CMakeFiles/pcregrep.dir/depend:
	cd /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35 /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35 /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcregrep.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/pcregrep.dir/depend

