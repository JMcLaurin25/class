# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_byte_order.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_byte_order.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_chartables.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_chartables.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_compile.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_compile.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_config.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_config.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_dfa_exec.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_dfa_exec.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_exec.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_exec.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_fullinfo.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_fullinfo.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_get.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_get.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_globals.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_globals.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_jit_compile.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_jit_compile.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_maketables.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_maketables.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_newline.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_newline.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_ord2utf8.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_ord2utf8.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_refcount.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_refcount.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_string_utils.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_string_utils.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_study.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_study.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_tables.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_tables.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_ucd.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_ucd.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_valid_utf8.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_valid_utf8.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_version.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_version.c.o"
  "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_xclass.c" "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/CMakeFiles/pcre.dir/pcre_xclass.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CONFIG_H"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  ".."
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
