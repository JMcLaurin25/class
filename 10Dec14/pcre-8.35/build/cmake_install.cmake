# Install script for directory: /home/ubuntu/workspace/mclaurin/cpp/pcre-8.35

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/libpcre.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/libpcreposix.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/libpcrecpp.a")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep"
         RPATH "")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcregrep")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcregrep")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest"
         RPATH "")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcretest")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcretest")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest"
         RPATH "")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcrecpp_unittest")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcrecpp_unittest")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest"
         RPATH "")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_scanner_unittest")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_scanner_unittest")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest")
    FILE(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest"
         RPATH "")
  ENDIF()
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/bin" TYPE EXECUTABLE FILES "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_stringpiece_unittest")
  IF(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest")
    IF(CMAKE_INSTALL_DO_STRIP)
      EXECUTE_PROCESS(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/bin/pcre_stringpiece_unittest")
    ENDIF(CMAKE_INSTALL_DO_STRIP)
  ENDIF()
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre.h"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcreposix.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcrecpp.h"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/pcre_scanner.h"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcrecpparg.h"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/pcre_stringpiece.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man1" TYPE FILE FILES
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcretest.1"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre-config.1"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcregrep.1"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/man/man3" TYPE FILE FILES
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrepattern.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcreapi.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrebuild.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcresyntax.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrecompat.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrelimits.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_config.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre32.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_get_substring_list.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcreprecompile.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcreperform.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_maketables.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_jit_stack_free.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_copy_named_substring.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrematching.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrestack.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcredemo.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_version.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcresample.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrejit.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_assign_jit_stack.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_get_stringtable_entries.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrecpp.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_compile.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_utf32_to_host_byte_order.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_pattern_to_host_byte_order.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_get_stringnumber.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrecallout.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_free_substring_list.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_refcount.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_dfa_exec.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_copy_substring.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcreposix.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_fullinfo.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_free_substring.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_get_named_substring.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_jit_stack_alloc.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre16.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_jit_exec.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_study.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcrepartial.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_free_study.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_compile2.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcreunicode.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_exec.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_utf16_to_host_byte_order.3"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/pcre_get_substring.3"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/pcre/html" TYPE FILE FILES
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre-config.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_pattern_to_host_byte_order.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_utf16_to_host_byte_order.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcresyntax.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_fullinfo.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcreunicode.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcreperform.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcreposix.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_free_substring.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_jit_stack_free.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrepattern.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_refcount.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrejit.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_jit_exec.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_copy_named_substring.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/index.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_compile.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_free_study.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcreapi.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcregrep.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrebuild.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_maketables.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_get_substring.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_compile2.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_assign_jit_stack.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrecompat.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre16.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_get_named_substring.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_dfa_exec.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_study.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_get_stringnumber.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre32.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrelimits.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrecallout.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_config.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_jit_stack_alloc.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_get_stringtable_entries.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrematching.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_version.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrecpp.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrepartial.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_free_substring_list.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_utf32_to_host_byte_order.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcresample.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_get_substring_list.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcreprecompile.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_copy_substring.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcretest.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcrestack.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcredemo.html"
    "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/doc/html/pcre_exec.html"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
ELSE(CMAKE_INSTALL_COMPONENT)
  SET(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
ENDIF(CMAKE_INSTALL_COMPONENT)

FILE(WRITE "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/${CMAKE_INSTALL_MANIFEST}" "")
FOREACH(file ${CMAKE_INSTALL_MANIFEST_FILES})
  FILE(APPEND "/home/ubuntu/workspace/mclaurin/cpp/pcre-8.35/build/${CMAKE_INSTALL_MANIFEST}" "${file}\n")
ENDFOREACH(file)
