################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../pcre-8.35/dftables.c \
../pcre-8.35/pcre16_byte_order.c \
../pcre-8.35/pcre16_chartables.c \
../pcre-8.35/pcre16_compile.c \
../pcre-8.35/pcre16_config.c \
../pcre-8.35/pcre16_dfa_exec.c \
../pcre-8.35/pcre16_exec.c \
../pcre-8.35/pcre16_fullinfo.c \
../pcre-8.35/pcre16_get.c \
../pcre-8.35/pcre16_globals.c \
../pcre-8.35/pcre16_jit_compile.c \
../pcre-8.35/pcre16_maketables.c \
../pcre-8.35/pcre16_newline.c \
../pcre-8.35/pcre16_ord2utf16.c \
../pcre-8.35/pcre16_printint.c \
../pcre-8.35/pcre16_refcount.c \
../pcre-8.35/pcre16_string_utils.c \
../pcre-8.35/pcre16_study.c \
../pcre-8.35/pcre16_tables.c \
../pcre-8.35/pcre16_ucd.c \
../pcre-8.35/pcre16_utf16_utils.c \
../pcre-8.35/pcre16_valid_utf16.c \
../pcre-8.35/pcre16_version.c \
../pcre-8.35/pcre16_xclass.c \
../pcre-8.35/pcre32_byte_order.c \
../pcre-8.35/pcre32_chartables.c \
../pcre-8.35/pcre32_compile.c \
../pcre-8.35/pcre32_config.c \
../pcre-8.35/pcre32_dfa_exec.c \
../pcre-8.35/pcre32_exec.c \
../pcre-8.35/pcre32_fullinfo.c \
../pcre-8.35/pcre32_get.c \
../pcre-8.35/pcre32_globals.c \
../pcre-8.35/pcre32_jit_compile.c \
../pcre-8.35/pcre32_maketables.c \
../pcre-8.35/pcre32_newline.c \
../pcre-8.35/pcre32_ord2utf32.c \
../pcre-8.35/pcre32_printint.c \
../pcre-8.35/pcre32_refcount.c \
../pcre-8.35/pcre32_string_utils.c \
../pcre-8.35/pcre32_study.c \
../pcre-8.35/pcre32_tables.c \
../pcre-8.35/pcre32_ucd.c \
../pcre-8.35/pcre32_utf32_utils.c \
../pcre-8.35/pcre32_valid_utf32.c \
../pcre-8.35/pcre32_version.c \
../pcre-8.35/pcre32_xclass.c \
../pcre-8.35/pcre_byte_order.c \
../pcre-8.35/pcre_compile.c \
../pcre-8.35/pcre_config.c \
../pcre-8.35/pcre_dfa_exec.c \
../pcre-8.35/pcre_exec.c \
../pcre-8.35/pcre_fullinfo.c \
../pcre-8.35/pcre_get.c \
../pcre-8.35/pcre_globals.c \
../pcre-8.35/pcre_jit_compile.c \
../pcre-8.35/pcre_jit_test.c \
../pcre-8.35/pcre_maketables.c \
../pcre-8.35/pcre_newline.c \
../pcre-8.35/pcre_ord2utf8.c \
../pcre-8.35/pcre_printint.c \
../pcre-8.35/pcre_refcount.c \
../pcre-8.35/pcre_string_utils.c \
../pcre-8.35/pcre_study.c \
../pcre-8.35/pcre_tables.c \
../pcre-8.35/pcre_ucd.c \
../pcre-8.35/pcre_valid_utf8.c \
../pcre-8.35/pcre_version.c \
../pcre-8.35/pcre_xclass.c \
../pcre-8.35/pcredemo.c \
../pcre-8.35/pcregrep.c \
../pcre-8.35/pcreposix.c \
../pcre-8.35/pcretest.c 

CC_SRCS += \
../pcre-8.35/pcre_scanner.cc \
../pcre-8.35/pcre_scanner_unittest.cc \
../pcre-8.35/pcre_stringpiece.cc \
../pcre-8.35/pcre_stringpiece_unittest.cc \
../pcre-8.35/pcrecpp.cc \
../pcre-8.35/pcrecpp_unittest.cc 

OBJS += \
./pcre-8.35/dftables.o \
./pcre-8.35/pcre16_byte_order.o \
./pcre-8.35/pcre16_chartables.o \
./pcre-8.35/pcre16_compile.o \
./pcre-8.35/pcre16_config.o \
./pcre-8.35/pcre16_dfa_exec.o \
./pcre-8.35/pcre16_exec.o \
./pcre-8.35/pcre16_fullinfo.o \
./pcre-8.35/pcre16_get.o \
./pcre-8.35/pcre16_globals.o \
./pcre-8.35/pcre16_jit_compile.o \
./pcre-8.35/pcre16_maketables.o \
./pcre-8.35/pcre16_newline.o \
./pcre-8.35/pcre16_ord2utf16.o \
./pcre-8.35/pcre16_printint.o \
./pcre-8.35/pcre16_refcount.o \
./pcre-8.35/pcre16_string_utils.o \
./pcre-8.35/pcre16_study.o \
./pcre-8.35/pcre16_tables.o \
./pcre-8.35/pcre16_ucd.o \
./pcre-8.35/pcre16_utf16_utils.o \
./pcre-8.35/pcre16_valid_utf16.o \
./pcre-8.35/pcre16_version.o \
./pcre-8.35/pcre16_xclass.o \
./pcre-8.35/pcre32_byte_order.o \
./pcre-8.35/pcre32_chartables.o \
./pcre-8.35/pcre32_compile.o \
./pcre-8.35/pcre32_config.o \
./pcre-8.35/pcre32_dfa_exec.o \
./pcre-8.35/pcre32_exec.o \
./pcre-8.35/pcre32_fullinfo.o \
./pcre-8.35/pcre32_get.o \
./pcre-8.35/pcre32_globals.o \
./pcre-8.35/pcre32_jit_compile.o \
./pcre-8.35/pcre32_maketables.o \
./pcre-8.35/pcre32_newline.o \
./pcre-8.35/pcre32_ord2utf32.o \
./pcre-8.35/pcre32_printint.o \
./pcre-8.35/pcre32_refcount.o \
./pcre-8.35/pcre32_string_utils.o \
./pcre-8.35/pcre32_study.o \
./pcre-8.35/pcre32_tables.o \
./pcre-8.35/pcre32_ucd.o \
./pcre-8.35/pcre32_utf32_utils.o \
./pcre-8.35/pcre32_valid_utf32.o \
./pcre-8.35/pcre32_version.o \
./pcre-8.35/pcre32_xclass.o \
./pcre-8.35/pcre_byte_order.o \
./pcre-8.35/pcre_compile.o \
./pcre-8.35/pcre_config.o \
./pcre-8.35/pcre_dfa_exec.o \
./pcre-8.35/pcre_exec.o \
./pcre-8.35/pcre_fullinfo.o \
./pcre-8.35/pcre_get.o \
./pcre-8.35/pcre_globals.o \
./pcre-8.35/pcre_jit_compile.o \
./pcre-8.35/pcre_jit_test.o \
./pcre-8.35/pcre_maketables.o \
./pcre-8.35/pcre_newline.o \
./pcre-8.35/pcre_ord2utf8.o \
./pcre-8.35/pcre_printint.o \
./pcre-8.35/pcre_refcount.o \
./pcre-8.35/pcre_scanner.o \
./pcre-8.35/pcre_scanner_unittest.o \
./pcre-8.35/pcre_string_utils.o \
./pcre-8.35/pcre_stringpiece.o \
./pcre-8.35/pcre_stringpiece_unittest.o \
./pcre-8.35/pcre_study.o \
./pcre-8.35/pcre_tables.o \
./pcre-8.35/pcre_ucd.o \
./pcre-8.35/pcre_valid_utf8.o \
./pcre-8.35/pcre_version.o \
./pcre-8.35/pcre_xclass.o \
./pcre-8.35/pcrecpp.o \
./pcre-8.35/pcrecpp_unittest.o \
./pcre-8.35/pcredemo.o \
./pcre-8.35/pcregrep.o \
./pcre-8.35/pcreposix.o \
./pcre-8.35/pcretest.o 

C_DEPS += \
./pcre-8.35/dftables.d \
./pcre-8.35/pcre16_byte_order.d \
./pcre-8.35/pcre16_chartables.d \
./pcre-8.35/pcre16_compile.d \
./pcre-8.35/pcre16_config.d \
./pcre-8.35/pcre16_dfa_exec.d \
./pcre-8.35/pcre16_exec.d \
./pcre-8.35/pcre16_fullinfo.d \
./pcre-8.35/pcre16_get.d \
./pcre-8.35/pcre16_globals.d \
./pcre-8.35/pcre16_jit_compile.d \
./pcre-8.35/pcre16_maketables.d \
./pcre-8.35/pcre16_newline.d \
./pcre-8.35/pcre16_ord2utf16.d \
./pcre-8.35/pcre16_printint.d \
./pcre-8.35/pcre16_refcount.d \
./pcre-8.35/pcre16_string_utils.d \
./pcre-8.35/pcre16_study.d \
./pcre-8.35/pcre16_tables.d \
./pcre-8.35/pcre16_ucd.d \
./pcre-8.35/pcre16_utf16_utils.d \
./pcre-8.35/pcre16_valid_utf16.d \
./pcre-8.35/pcre16_version.d \
./pcre-8.35/pcre16_xclass.d \
./pcre-8.35/pcre32_byte_order.d \
./pcre-8.35/pcre32_chartables.d \
./pcre-8.35/pcre32_compile.d \
./pcre-8.35/pcre32_config.d \
./pcre-8.35/pcre32_dfa_exec.d \
./pcre-8.35/pcre32_exec.d \
./pcre-8.35/pcre32_fullinfo.d \
./pcre-8.35/pcre32_get.d \
./pcre-8.35/pcre32_globals.d \
./pcre-8.35/pcre32_jit_compile.d \
./pcre-8.35/pcre32_maketables.d \
./pcre-8.35/pcre32_newline.d \
./pcre-8.35/pcre32_ord2utf32.d \
./pcre-8.35/pcre32_printint.d \
./pcre-8.35/pcre32_refcount.d \
./pcre-8.35/pcre32_string_utils.d \
./pcre-8.35/pcre32_study.d \
./pcre-8.35/pcre32_tables.d \
./pcre-8.35/pcre32_ucd.d \
./pcre-8.35/pcre32_utf32_utils.d \
./pcre-8.35/pcre32_valid_utf32.d \
./pcre-8.35/pcre32_version.d \
./pcre-8.35/pcre32_xclass.d \
./pcre-8.35/pcre_byte_order.d \
./pcre-8.35/pcre_compile.d \
./pcre-8.35/pcre_config.d \
./pcre-8.35/pcre_dfa_exec.d \
./pcre-8.35/pcre_exec.d \
./pcre-8.35/pcre_fullinfo.d \
./pcre-8.35/pcre_get.d \
./pcre-8.35/pcre_globals.d \
./pcre-8.35/pcre_jit_compile.d \
./pcre-8.35/pcre_jit_test.d \
./pcre-8.35/pcre_maketables.d \
./pcre-8.35/pcre_newline.d \
./pcre-8.35/pcre_ord2utf8.d \
./pcre-8.35/pcre_printint.d \
./pcre-8.35/pcre_refcount.d \
./pcre-8.35/pcre_string_utils.d \
./pcre-8.35/pcre_study.d \
./pcre-8.35/pcre_tables.d \
./pcre-8.35/pcre_ucd.d \
./pcre-8.35/pcre_valid_utf8.d \
./pcre-8.35/pcre_version.d \
./pcre-8.35/pcre_xclass.d \
./pcre-8.35/pcredemo.d \
./pcre-8.35/pcregrep.d \
./pcre-8.35/pcreposix.d \
./pcre-8.35/pcretest.d 

CC_DEPS += \
./pcre-8.35/pcre_scanner.d \
./pcre-8.35/pcre_scanner_unittest.d \
./pcre-8.35/pcre_stringpiece.d \
./pcre-8.35/pcre_stringpiece_unittest.d \
./pcre-8.35/pcrecpp.d \
./pcre-8.35/pcrecpp_unittest.d 


# Each subdirectory must supply rules for building sources it contributes
pcre-8.35/%.o: ../pcre-8.35/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

pcre-8.35/%.o: ../pcre-8.35/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I/home/jj/circleplot/class/10Dec14 -I/home/jj/circleplot/class/10Dec14/pcre-8.35 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


