################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../pcre-8.35/sljit/sljitExecAllocator.c \
../pcre-8.35/sljit/sljitLir.c \
../pcre-8.35/sljit/sljitNativeARM_32.c \
../pcre-8.35/sljit/sljitNativeARM_64.c \
../pcre-8.35/sljit/sljitNativeARM_T2_32.c \
../pcre-8.35/sljit/sljitNativeMIPS_32.c \
../pcre-8.35/sljit/sljitNativeMIPS_64.c \
../pcre-8.35/sljit/sljitNativeMIPS_common.c \
../pcre-8.35/sljit/sljitNativePPC_32.c \
../pcre-8.35/sljit/sljitNativePPC_64.c \
../pcre-8.35/sljit/sljitNativePPC_common.c \
../pcre-8.35/sljit/sljitNativeSPARC_32.c \
../pcre-8.35/sljit/sljitNativeSPARC_common.c \
../pcre-8.35/sljit/sljitNativeTILEGX-encoder.c \
../pcre-8.35/sljit/sljitNativeTILEGX_64.c \
../pcre-8.35/sljit/sljitNativeX86_32.c \
../pcre-8.35/sljit/sljitNativeX86_64.c \
../pcre-8.35/sljit/sljitNativeX86_common.c \
../pcre-8.35/sljit/sljitUtils.c 

OBJS += \
./pcre-8.35/sljit/sljitExecAllocator.o \
./pcre-8.35/sljit/sljitLir.o \
./pcre-8.35/sljit/sljitNativeARM_32.o \
./pcre-8.35/sljit/sljitNativeARM_64.o \
./pcre-8.35/sljit/sljitNativeARM_T2_32.o \
./pcre-8.35/sljit/sljitNativeMIPS_32.o \
./pcre-8.35/sljit/sljitNativeMIPS_64.o \
./pcre-8.35/sljit/sljitNativeMIPS_common.o \
./pcre-8.35/sljit/sljitNativePPC_32.o \
./pcre-8.35/sljit/sljitNativePPC_64.o \
./pcre-8.35/sljit/sljitNativePPC_common.o \
./pcre-8.35/sljit/sljitNativeSPARC_32.o \
./pcre-8.35/sljit/sljitNativeSPARC_common.o \
./pcre-8.35/sljit/sljitNativeTILEGX-encoder.o \
./pcre-8.35/sljit/sljitNativeTILEGX_64.o \
./pcre-8.35/sljit/sljitNativeX86_32.o \
./pcre-8.35/sljit/sljitNativeX86_64.o \
./pcre-8.35/sljit/sljitNativeX86_common.o \
./pcre-8.35/sljit/sljitUtils.o 

C_DEPS += \
./pcre-8.35/sljit/sljitExecAllocator.d \
./pcre-8.35/sljit/sljitLir.d \
./pcre-8.35/sljit/sljitNativeARM_32.d \
./pcre-8.35/sljit/sljitNativeARM_64.d \
./pcre-8.35/sljit/sljitNativeARM_T2_32.d \
./pcre-8.35/sljit/sljitNativeMIPS_32.d \
./pcre-8.35/sljit/sljitNativeMIPS_64.d \
./pcre-8.35/sljit/sljitNativeMIPS_common.d \
./pcre-8.35/sljit/sljitNativePPC_32.d \
./pcre-8.35/sljit/sljitNativePPC_64.d \
./pcre-8.35/sljit/sljitNativePPC_common.d \
./pcre-8.35/sljit/sljitNativeSPARC_32.d \
./pcre-8.35/sljit/sljitNativeSPARC_common.d \
./pcre-8.35/sljit/sljitNativeTILEGX-encoder.d \
./pcre-8.35/sljit/sljitNativeTILEGX_64.d \
./pcre-8.35/sljit/sljitNativeX86_32.d \
./pcre-8.35/sljit/sljitNativeX86_64.d \
./pcre-8.35/sljit/sljitNativeX86_common.d \
./pcre-8.35/sljit/sljitUtils.d 


# Each subdirectory must supply rules for building sources it contributes
pcre-8.35/sljit/%.o: ../pcre-8.35/sljit/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


