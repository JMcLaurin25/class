#ifndef WREGEX_H_
#define WREGEX_H_
#include <string>
#include <vector>

std::vector<std::string> search(const std::string &regex_str, const std::string &line);

#endif
